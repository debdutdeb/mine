call plug#begin("~/.nvim/plugged")

Plug 'flazz/vim-colorschemes'
Plug 'fatih/vim-go', { 'do': ':GoUpdateBinaries' }
Plug 'neoclide/coc.nvim', {'branch': 'release'}
Plug 'z0mbix/vim-shfmt', { 'for': 'sh' }
Plug 'editorconfig/editorconfig-vim'

call plug#end()

syntax on
set number
set wrap
set encoding=utf-8
set shiftwidth=4
set softtabstop=4
set tabstop=4
set number relativenumber
colorscheme molokai

let g:shfmt_fmt_on_save = 1
let g:shfmt_extra_args = '-w -bn -ci -sr'
