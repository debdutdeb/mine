#!/usr/bin/env bash

# Because only the cargo package is maintained officially :/
# Only for Ubuntu though

sudo apt update && \
    sudo apt install cargo cmake pkg-config libfreetype6-dev libfontconfig1-dev libxcb-xfixes0-dev python3 -y

cargo install alacritty
sudo wget https://github.com/alacritty/alacritty/releases/download/v0.5.0/Alacritty.desktop -O /usr/share/applications/Alacritty.desktop


echo 'PATH=$PATH:/home/debdut/.cargo/bin' >> ~/.bashrc
